﻿using System;
using System.Runtime.CompilerServices;
using System.Web.Mvc;
using Telerik.ReportViewer.Mvc;
namespace QClient
{
    public static class HtmlHelpers
    {
        // Summary:
        //     A factory used to generate the javascript representation of Telerik Reporting
        //     widgets
        //
        // Parameters:
        //   htmlHelper:
        //
        // Returns:
        //     Telerik.ReportViewer.Mvc.TelerikReportingFactory

        public static dynamic TelerikReporting(this HtmlHelper htmlHelper, string Reporting)
        {
            Telerik.Reporting.TypeReportSource typeReportSource = new Telerik.Reporting.TypeReportSource() { TypeName = Reporting };
            typeReportSource.Parameters.Add("id", 200);
            return Telerik.ReportViewer.Mvc.HtmlHelpers.TelerikReporting(htmlHelper).ReportViewer()
             .Id("reportViewer1")
             .ServiceUrl("../api/ServiceReports/")
             .TemplateUrl("../ReportViewer/templates/telerikReportViewerTemplate.html")
             .ReportSource(typeReportSource)
             .ViewMode(0)
             .ScaleMode(ScaleModes.SPECIFIC)
             .Scale(1.0)
             .PersistSession(true);
             // Telerik.ReportViewer.Mvc.HtmlHelpers.TelerikReporting(htmlHelper).ReportViewer ();
             
        }
       
    }
}