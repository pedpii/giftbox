﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Models
{
    public class UserModel
    {
        public int ID { get; set; }
        [DisplayName("Username")]
        [Required(ErrorMessage = "The field is Required")]
        public string Username { get; set; }
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "The field is Required")]
        public string Password { get; set; }
        public string Name { get; set; }
        public int Status { get; set; }
        public string LoginErrorMsg { get; set; }
    }

    public class SelectionModel
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public List<int> GiverID { get; set; }
        public List<int> ReceiverID { get; set; }
        public List<bool> Status { get; set; }
    }
    public class SelectionShow
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public List<int> GiverID { get; set; }
        public List<string> GiverName { get; set; }
        public List<int> ReceiverID { get; set; }
        public List<string> ReceiverName { get; set; }
        public List<bool> Status { get; set; }
    }
    public class AnswerModel
    {
        public int UserID { get; set; }
        public int ReceiverID { get; set; }
    }

    public class ShowModel
    {
        public int UserID { get; set; }
        public string Name { get; set; }
        public int income { get; set; }
        public int exp { get; set; }
        public int total { get; set; }
    }

    public class IncomeModel
    {
        public int UserID { get; set; }
        public string Name { get; set; }
        public int Correct { get; set; }
    }

    public class EXPModel
    {
        public int UserID { get; set; }
        public string Name { get; set; }
        public int exp { get; set; }
    }
    public class AnsAdminModel
    {
        public string Giver { get; set; }
        public string Receiver { get; set; }
    }
}

