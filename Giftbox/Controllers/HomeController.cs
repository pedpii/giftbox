﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web;
using CHO;
using System.Threading;
using System.Web.Mvc;
using System.Net.Sockets;
using CHO.Socket;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Web.Script.Serialization;
using Models;

namespace Giftbox.Controllers
{

    public class HomeController : Controller
    {
        //string SettingPath = System.Configuration.ConfigurationManager.AppSettings["SettingPath"].ToString() + "/Setting.ini";
        public ActionResult Index(SelectionModel selection)
        {
            using (GiftboxEntities db = new GiftboxEntities())
            {
                int IDUser = HttpContext.Session["IDUser"] == null ? 0 : (int)HttpContext.Session["IDUser"];
                if (IDUser <= 0)
                {
                    return RedirectToAction("Index", "Login");
                }

                var DBUser = db.User.Where(m => m.ID != IDUser && m.Status == 1).AsQueryable();
                List<UserModel> UserList = new List<UserModel>();
                ViewBag.ListUser = DBUser.ToList().Select(EntityToModel).ToList();
                //ViewBag.ListUser = db.User.ToList().Select(EntityToModel).ToList();
                var ansUser = db.Answer.Where(m => m.UserID == IDUser).FirstOrDefault();

                var userDetail = db.Selection.Where(m => m.UserID == IDUser).FirstOrDefault();

                if (userDetail == null)
                {
                    ViewBag.Meg = 0;
                }
                else
                {
                    ViewBag.Meg = 1;
                }
                if (ansUser == null)
                {
                    ViewBag.ans = 0;
                    ViewBag.ansed = " ";
                }
                else
                {
                    ViewBag.ansed = db.User.Where(m => m.ID == ansUser.ReceiverID).FirstOrDefault().Name;
                    ViewBag.ans = 1;
                }


            }
            return View();
        }
        public ActionResult AnswerSand(AnswerModel ans)
        {
            using (GiftboxEntities db = new GiftboxEntities())
            {
                int IDUser = HttpContext.Session["IDUser"] == null ? 0 : (int)HttpContext.Session["IDUser"];
                if (IDUser <= 0)
                {
                    return RedirectToAction("Index", "Login");
                }

                ViewBag.ListUser = db.User.ToList().Select(EntityToModel).ToList();
                var ansUser = db.Answer.Where(m => m.UserID == IDUser).FirstOrDefault();
                if (ansUser == null)
                {
                    db.Answer.Add(new Answer
                    {
                        UserID = ans.UserID,
                        ReceiverID = ans.ReceiverID,
                    });
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            return RedirectToAction("Index", "Home");
        }
        private UserModel EntityToModel(User Value)
        {
            return new UserModel()
            {
                ID = Value.ID,
                Name = Value.Name,
                Username = Value.Username,
                Password = Value.Password,
                Status = Value.Status
            };
        }

        public ActionResult Edit(Answer ans)
        {
            using (GiftboxEntities db = new GiftboxEntities())
            {
                var Item = db.Answer.Find(ans.UserID);
                if (Item != null)
                {
                    var GiverID = db.Answer.FirstOrDefault(s => s.UserID == ans.UserID);
                    if (GiverID != null)
                    {
                        GiverID.ReceiverID = ans.ReceiverID;
                    }
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {

                    }
                }
                //
            }
            return RedirectToAction("Index", "Home");
        }
    }


}


