﻿using Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Giftbox.Controllers
{
    public class PredictController : Controller
    {
        // GET: Predict
        public ActionResult Index()
        {
            //
            int IDUser = HttpContext.Session["IDUser"] == null ? 0 : (int)HttpContext.Session["IDUser"];
            if (IDUser <= 0)
            {
                return RedirectToAction("Index", "Login");
            }
            GiftboxEntities db = new GiftboxEntities();
            var DBUser = db.User.Where(m => m.ID != IDUser && m.Status == 1).AsQueryable();
            List<UserModel> UserList = new List<UserModel>();
            UserList = DBUser.ToList().Select(EntityToModel).ToList();

            ViewBag.ListUser = db.User.Where(m => m.Status == 1).ToList().Select(EntityToModel).ToList();
            return PartialView(UserList);
        }
        private UserModel EntityToModel(User Value)
        {
            return new UserModel()
            {
                ID = Value.ID,
                Name = Value.Name,
                Username = Value.Username,
                Password = Value.Password,
                Status = Value.Status
            };
        }
        [HttpPost]
        public ActionResult Selection(SelectionModel Value)
        {
            using (GiftboxEntities db = new GiftboxEntities())
            {
                int IDUser = HttpContext.Session["IDUser"] == null ? 0 : (int)HttpContext.Session["IDUser"];
                if (IDUser <= 0)
                {
                    return RedirectToAction("Index", "Login");
                }

                for (int i = 0; i <= Value.GiverID.Count - 1; i++)
                {

                    db.Selection.Add(new Selection
                    {
                        GiverID = Value.GiverID[i],
                        ReceiverID = Value.ReceiverID[i],
                        UserID = IDUser,
                        Status = false,
                    });
                }
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {

                }
            }
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Results()
        {
            SelectionShow UserList = new SelectionShow();
            using (GiftboxEntities db = new GiftboxEntities())
            {
                int IDUser = HttpContext.Session["IDUser"] == null ? 0 : (int)HttpContext.Session["IDUser"];
                if (IDUser <= 0)
                {
                    return RedirectToAction("Index", "Login");
                }
                //income
                //check Userid How true count it : Answer Inner join selection
                //expenses
                //check Userid in Answer DB : check in Selection true count it



               

                var ansFilter_f = (from ans in db.Answer
                                   join sel in db.Selection
                                   on ans.UserID equals sel.GiverID
                                   where ans.UserID == sel.GiverID && ans.ReceiverID == sel.ReceiverID
                                   orderby ans.UserID ascending
                                   select new { GiverID = ans.UserID, ReceiverID = sel.ReceiverID, UserID = sel.UserID });

                var DBansFilter = ansFilter_f.Where(m => m.UserID == IDUser).ToList();

                //var test = DBansFilter.GroupBy(s => s.UserID).Select(x => new
                //{
                //    Userid = x.Key,
                //    giveId = x.Select(s=> new { Ans =s.GiverID }).ToList(),
                //    receiverID = x.Select(s => new { Ans = s.ReceiverID }).ToList()
                //}).ToList();

                //var exp = DBansFilter.GroupBy(s => s.GiverID).Select(x => new
                //{
                //    Userid = x.Key,
                //    giveId = x.Select(s => new { Ans = s.UserID }).ToList(),
                //    receiverID = x.Select(s => new { Ans = s.ReceiverID }).ToList()
                //}).ToList();

                var UserLists = db.Selection.Where(m => m.UserID == IDUser).AsQueryable().ToList();

                UserList.ID = UserLists.FirstOrDefault().ID;
                UserList.Status = new List<bool>();
                UserList.UserID = UserLists.FirstOrDefault().UserID;
                UserList.GiverID = new List<int>();
                UserList.ReceiverID = new List<int>();
                UserList.GiverName = new List<string>();
                UserList.ReceiverName = new List<string>();


                foreach (var item in UserLists)
                {
                    string gatGiverName = db.User.Where(w => w.ID == item.GiverID).FirstOrDefault().Name;
                    UserList.GiverName.Add(gatGiverName);
                    string gatReceiverName = db.User.Where(w => w.ID == item.ReceiverID).FirstOrDefault().Name;
                    UserList.ReceiverName.Add(gatReceiverName);
                    if (DBansFilter.Any(w => w.GiverID == item.GiverID) && DBansFilter.Any(w => w.ReceiverID == item.ReceiverID))
                    {
                        UserList.Status.Add(true);
                        var FindID = db.Selection.FirstOrDefault(w => w.ID == item.ID && w.UserID == item.UserID);
                        if (FindID != null)
                        {
                            FindID.Status = true;
                        }
                        try
                        {
                            db.SaveChanges();
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    else
                    {
                        UserList.Status.Add(item.Status);
                    }
                }

                int money = db.Selection.Where(w => w.Status == true && w.UserID == IDUser).Count();
                ViewBag.money = money;
                ViewBag.total = money * 20;
            }
            return View(UserList);
        }

    }
}