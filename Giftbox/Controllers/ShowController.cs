﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Giftbox.Controllers
{
    public class ShowController : Controller
    {
        // GET: Show
        public ActionResult Index()
        {
            GiftboxEntities db = new GiftboxEntities();
            int IDUser = HttpContext.Session["IDUser"] == null ? 0 : (int)HttpContext.Session["IDUser"];
            if (IDUser <= 0)
            {
                return RedirectToAction("Index", "Login");
            }
            var DataList = db.User.ToList();

            var DBSelection = db.Selection.AsQueryable();

            var ansFilter_f = (from ans in db.Answer
                               join sel in db.Selection
                               on ans.UserID equals sel.GiverID
                               where ans.UserID == sel.GiverID && ans.ReceiverID == sel.ReceiverID
                               orderby ans.UserID ascending
                               select new { GiverID = ans.UserID, ReceiverID = sel.ReceiverID, UserID = sel.UserID });

            var DBansFilter = ansFilter_f.ToList();

            var income = DBansFilter.GroupBy(s => s.UserID).Select(x => new 
            {
                UserID = x.Key,
                Name = db.User.Where(m => m.ID == x.Key).FirstOrDefault().Name,
                Correct = x.Select(s => new { GiverID = s.GiverID }).Count() * 20,
                exp = 0
            }).ToList();

            ViewBag.DataList = income;

            var exp = DBansFilter.GroupBy(s => s.GiverID).Select(x => new 
            {
                UserID = x.Key,
                Name = db.User.Where(m => m.ID == x.Key).FirstOrDefault().Name,
                Correct = 0,
                exp = x.Select(s => new { ReceiverID = s.ReceiverID }).Count() * 20,
            }).ToList();
            ViewBag.DataList = exp;

            //var InandExp = income.GroupJoin(exp, inc => inc.UserID, expgj => expgj.UserID, (inc, expgj) => new ShowModel
            //{
            //    UserID = inc.UserID,
            //    Name = inc.Name,
            //    income = inc.Correct,
            //    exp = expgj.Select(s => s.exp).FirstOrDefault(),
            //    total = inc.Correct - expgj.Select(s => s.exp).FirstOrDefault()
            //}).ToList();
            var InandExp = income.Union(exp).GroupBy(g => g.UserID).Select(s => new ShowModel
            {
                UserID = s.Key,
                Name = s.Select(se => se.Name).FirstOrDefault(),
                income = s.Sum(su => su.Correct),
                exp = s.Sum(su => su.exp),
                total = s.Sum(su => su.Correct) - s.Sum(su => su.exp)
            }).ToList();

            return View(InandExp);

        }
    }
}