﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Giftbox.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(UserModel user)
        {
            using (GiftboxEntities db = new GiftboxEntities())
            {
                var userDetail = db.User.Where(m => m.Username == user.Username && m.Password == user.Password).FirstOrDefault();
                if (userDetail == null)
                {
                    user.LoginErrorMsg = "Invaild UserName or Password";
                    return View("Index", user);
                }
                if (userDetail.Status < 1)
                {
                    user.LoginErrorMsg = "Invaild UserName or Password"; return View("Index", user);
                }

                else
                {
                    if (userDetail.Status == 5)
                    {
                        Session["IDUser"] = userDetail.ID;
                        return RedirectToAction("Index", "User");
                    }

                    Session["IDUser"] = userDetail.ID;
                    Session["Username"] = userDetail.Username;
                    Session["Name"] = userDetail.Name;
                    Session["Status"] = userDetail.Status;

                    return RedirectToAction("Index", "Home");
                }
            }
        }
        public ActionResult Logout()
        {
            int IDUser = (int)Session["IDUser"];
            Session.Abandon();
            return RedirectToAction("Index", "Login");
        }
    }
}