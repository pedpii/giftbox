﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Giftbox.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Index(User ID)
        {
            int IDUser = HttpContext.Session["IDUser"] == null ? 0 : (int)HttpContext.Session["IDUser"];
            GiftboxEntities db = new GiftboxEntities();
            var IDUser_Status = db.User.FirstOrDefault(s => s.ID == IDUser);
            if (IDUser <= 0 || IDUser_Status.Status != 5)
            {
                return RedirectToAction("Index", "Login");
            }
            var DataList = db.User.ToList();

            var DBSelection = db.Selection.AsQueryable();

            var ansFilter_f = (from ans in db.Answer
                               join sel in db.Selection
                               on ans.UserID equals sel.GiverID
                               where ans.UserID == sel.GiverID && ans.ReceiverID == sel.ReceiverID
                               orderby ans.UserID ascending
                               select new { GiverID = ans.UserID, ReceiverID = sel.ReceiverID, UserID = sel.UserID });

            var DBansFilter = ansFilter_f.ToList();

            var income = DBansFilter.GroupBy(s => s.UserID).Select(x => new 
            {
                UserID = x.Key,
                Name = db.User.Where(m => m.ID == x.Key).FirstOrDefault().Name,
                Correct = x.Select(s => new { GiverID = s.GiverID }).Count()*20,
                exp = 0
            }).ToList();

            ViewBag.DataList = income;

            var exp = DBansFilter.GroupBy(s => s.GiverID).Select(x => new 
            {
                UserID = x.Key,
                Name = db.User.Where(m => m.ID == x.Key).FirstOrDefault().Name,
                Correct = 0,
                exp = x.Select(s => new { ReceiverID = s.ReceiverID }).Count()*20,
            }).ToList();
            ViewBag.DataList = exp;

            //var InandExp = income.GroupJoin(exp, inc => inc.UserID, expgj => expgj.UserID, (inc, expgj) => new ShowModel
            //{
            //    UserID = inc.UserID,
            //    Name = inc.Name,
            //    income = inc.Correct,
            //    exp = expgj.Select(s => s.exp).FirstOrDefault(),
            //    total = inc.Correct - expgj.Select(s => s.exp).FirstOrDefault()
            //}).ToList();

            var InandExp = income.Union(exp).GroupBy(g => g.UserID).Select(s => new ShowModel
            {
                UserID = s.Key,
                Name = s.Select(se => se.Name).FirstOrDefault(),
                income = s.Sum(su => su.Correct),
                exp = s.Sum(su => su.exp),
                total = s.Sum(su => su.Correct) - s.Sum(su => su.exp)
            }).ToList();


            return View(InandExp);
        }
        public ActionResult Answer(Answer data)
        {
            using (GiftboxEntities db = new GiftboxEntities())
            {
                int IDUser = HttpContext.Session["IDUser"] == null ? 0 : (int)HttpContext.Session["IDUser"];
                if (IDUser <= 0)
                {
                    return RedirectToAction("Index", "Login");
                }
                
                    var DataList = db.Answer.ToList();
                    var exp = DataList.FindAll(s => s.UserID >= 1).Select(x => new AnsAdminModel
                    {
                        Giver = db.User.Where(m => m.ID == x.UserID).FirstOrDefault().Name,
                        Receiver = db.User.Where(m => m.ID == x.ReceiverID).FirstOrDefault().Name,
                    }).ToList();
                ViewBag.DataList = exp;
                
            }
                return View();
        }
        public ActionResult Detail(int id)
        {
            SelectionShow UserList = new SelectionShow();
            using (GiftboxEntities db = new GiftboxEntities())
            {
                int IDUser = HttpContext.Session["IDUser"] == null ? 0 : (int)HttpContext.Session["IDUser"];
                if (IDUser <= 0)
                {
                    return RedirectToAction("Index", "Login");
                }

                var ansFilter_f = (from ans in db.Answer
                                   join sel in db.Selection
                                   on ans.UserID equals sel.GiverID
                                   where ans.UserID == sel.GiverID && ans.ReceiverID == sel.ReceiverID
                                   orderby ans.UserID ascending
                                   select new { GiverID = ans.UserID, ReceiverID = sel.ReceiverID, UserID = sel.UserID });

                var DBansFilter = ansFilter_f.Where(m => m.UserID == id).ToList();
                

                var UserLists = db.Selection.Where(m => m.UserID == id).AsQueryable().ToList();

                UserList.ID = UserLists.FirstOrDefault().ID;
                UserList.Status = new List<bool>();
                UserList.UserID = UserLists.FirstOrDefault().UserID;
                UserList.GiverID = new List<int>();
                UserList.ReceiverID = new List<int>();
                UserList.GiverName = new List<string>();
                UserList.ReceiverName = new List<string>();


                foreach (var item in UserLists)
                {
                    string gatGiverName = db.User.Where(w => w.ID == item.GiverID).FirstOrDefault().Name;
                    UserList.GiverName.Add(gatGiverName);
                    string gatReceiverName = db.User.Where(w => w.ID == item.ReceiverID).FirstOrDefault().Name;
                    UserList.ReceiverName.Add(gatReceiverName);
                    if (DBansFilter.Any(w => w.GiverID == item.GiverID) && DBansFilter.Any(w => w.ReceiverID == item.ReceiverID))
                    {
                        UserList.Status.Add(true);
                        var FindID = db.Selection.FirstOrDefault(w => w.ID == item.ID && w.UserID == item.UserID);
                        if (FindID != null)
                        {
                            FindID.Status = true;
                        }
                        try
                        {
                            db.SaveChanges();
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    else
                    {
                        UserList.Status.Add(item.Status);
                    }
                }
                ViewBag.Name = db.User.Where(w => w.ID == id).FirstOrDefault().Name;
            }
                return View(UserList);
        }
        public ActionResult Delete(int id)
        {

            using (GiftboxEntities db = new GiftboxEntities())
            {
                var Item = db.User.Find(id);
                if (Item != null)
                {
                    var Delete = db.User.FirstOrDefault(s => s.ID == id);
                    if (Delete != null)
                    {
                        Delete.Status = 0;
                    }
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {

                    }
                    ViewBag.MSG = Item.Name;
                }
                return RedirectToAction("Index","User");
            }
        }
    }
}