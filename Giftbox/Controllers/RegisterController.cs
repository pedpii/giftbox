﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Giftbox.Controllers
{
    public class RegisterController : Controller
    {
        // GET: Register
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(UserModel item)
        {

            using (GiftboxEntities db = new GiftboxEntities())
            {
                if (!String.IsNullOrEmpty(item.Username) && !String.IsNullOrEmpty(item.Password))
                {
                    
                    var userDetail = db.User.Where(m => m.Username == item.Username && m.Password == item.Password).FirstOrDefault();

                    if (userDetail != null)
                    {
                        if (userDetail.Status > 0)
                        {
                            item.LoginErrorMsg = "Have account";
                            return View("Index", item);
                        }
                        else
                        {
                            userDetail.Status = 1;
                        }

                    }
                    else
                    {
                        db.User.Add(new User
                        {
                            ID = db.User.Count() + 1,
                            Username = item.Username,
                            Password = item.Password,
                            Name = item.Name,
                            Status = 1
                        });

                    }
                    try
                    {
                        db.SaveChanges();
                        Session["IDUser"] = item.ID;
                        Session["Username"] = item.Username;
                        Session["Name"] = item.Name;
                        return RedirectToAction("Index", "Home");
                    }
                    catch (Exception ex)
                    {
                        item.LoginErrorMsg = ex.InnerException.ToString();
                        return View("Index", item);
                    }
                }
                else
                {
                    item.LoginErrorMsg = "Invaild UserName or Password";
                    return View("Index", item);
                }
                
            return RedirectToAction("Index", "Home");
        }
    }
}
}